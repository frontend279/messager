import React from 'react';
import {
	Avatar,
	AvatarGroup,
	Box, Divider,
	ImageList, ImageListItem,
	List, ListItem, ListItemAvatar,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Typography
} from "@mui/material";


const RecentActions = () => {
	return (
		<Box flex={4} sx={{display: {xs: 'none', sm: 'block'}}}>
			<Box position="fixed" mt={2} width={300}>
				<Typography variant="h6" mb={2}>
					Друзья онлайн
				</Typography>
				<AvatarGroup max={6}>
					<Avatar alt="Remy Sharp" src="https://mui.com/static/images/avatar/1.jpg"/>
					<Avatar alt="Travis Howard" src="https://mui.com/static/images/avatar/2.jpg"/>
					<Avatar alt="Cindy Baker" src="https://mui.com/static/images/avatar/3.jpg"/>
					<Avatar alt="Agnes Walker" src="https://mui.com/static/images/avatar/4.jpg"/>
					<Avatar alt="Trevor Henderson" src="https://mui.com/static/images/avatar/5.jpg"/>
					<Avatar alt="Trevor Henderson" src="https://mui.com/static/images/avatar/5.jpg"/>
					<Avatar alt="Trevor Henderson" src="/static/images/avatar/5.jpg"/>
				</AvatarGroup>

				<Typography variant="h6" mt={4} mb={2}>
					Фотографии
				</Typography>
				<ImageList cols={3} rowHeight={100} gap={5}>
					<ImageListItem>
						<img
							src={`https://material-ui.com/static/images/image-list/breakfast.jpg`}
							alt={''}
						/>
					</ImageListItem>
					<ImageListItem>
						<img
							src={`https://material-ui.com/static/images/image-list/breakfast.jpg`}
							alt={''}
						/>
					</ImageListItem>
					<ImageListItem>
						<img
							src={`https://material-ui.com/static/images/image-list/breakfast.jpg`}
							alt={''}
						/>
					</ImageListItem>
				</ImageList>
				<Typography variant="h6" mt={4} mb={2}>
					Последние диалоги
				</Typography>
				<List>
					<ListItem alignItems="flex-start">
						<ListItemAvatar>
							<Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg"/>
						</ListItemAvatar>
						<ListItemText
							primary="Brunch this weekend?"
							secondary={
								<React.Fragment>
									<Typography
										sx={{display: 'inline'}}
									>
										Ali Connors
									</Typography>
									{" — I'll be in your neighborhood doing errands this…"}
								</React.Fragment>
							}
						/>
					</ListItem>
					<Divider variant="inset" />
					<ListItem alignItems="flex-start">
						<ListItemAvatar>
							<Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg"/>
						</ListItemAvatar>
						<ListItemText
							primary="Brunch this weekend?"
							secondary={
								<React.Fragment>
									<Typography
										sx={{display: 'inline'}}
									>
										Ali Connors
									</Typography>
									{" — I'll be in your neighborhood doing errands this…"}
								</React.Fragment>
							}
						/>
					</ListItem>
					<Divider variant="inset" />
					<ListItem alignItems="flex-start">
						<ListItemAvatar>
							<Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg"/>
						</ListItemAvatar>
						<ListItemText
							primary="Brunch this weekend?"
							secondary={
								<React.Fragment>
									<Typography
										sx={{display: 'inline'}}
									>
										Ali Connors
									</Typography>
									{" — I'll be in your neighborhood doing errands this…"}
								</React.Fragment>
							}
						/>
					</ListItem>
					<Divider variant="inset" />
				</List>
			</Box>

		</Box>
	);
};

export default RecentActions;