import {Box, Modal, styled} from "@mui/material";

export const CustomModalBox = styled(Box)({
	width: "50%",
	padding: "2rem",
	borderRadius: '5px',
	backgroundColor: 'white',
	display: "flex",
	flexDirection: "column",
	gap: "1rem"
})

export const CustomButtonsBox = styled(Box)({
	display: "flex",
	flexDirection: "row",
	justifyContent: "space-between",
	padding: '0 1rem'
})
export const CustomModal = styled(Modal)({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
})