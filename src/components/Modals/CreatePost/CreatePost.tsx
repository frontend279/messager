import React, {FC, useState} from 'react';
import {useAppDispatch, useAppSelector} from "../../../redux/store";
import {Box, Button, Modal, TextField, Typography} from "@mui/material";
import {CustomButtonsBox, CustomModal, CustomModalBox} from "./style";
import {CreatePostModalSliceActions} from "../../../redux/slices/modals/CreatePostSlice";

const CreatePost: FC = () => {
	
	const {isOpen} = useAppSelector(state => state.ModalCreatePostSlice)
	const dispatch = useAppDispatch()
	const [title, setTitle] = useState<string>('')
	const [body, setBody] = useState<string>('')
	const handleClose = () => {
		dispatch(CreatePostModalSliceActions.setModalOpen(false))
	}
	
	const handleSubmit = () => {
		const data = {
			title,
			body,
		}
		console.info('Отправка данных на сервер...')
		console.info(data)
		//здесь логика отправки
		setBody('')
		setTitle('')
		handleClose()			
	}
	
	return (
			<CustomModal
				open={isOpen}
				onClose={handleClose}
			>
				<CustomModalBox>
					<Typography variant={'h4'}>Создать пост</Typography>
					<TextField label="Введите название поста" variant="outlined"
										 value={title}
										 onChange={(e) => setTitle(e.target.value)}/>
					<TextField
						label="Что вы хотите нам рассказать?"
						multiline
						variant="outlined"
						rows={8}
						value={body}
						onChange={(e) => setBody(e.target.value)}
					/>
					<CustomButtonsBox>
						<Button size="large" variant="outlined" onClick={handleClose} color="error">Закрыть</Button>
						<Button size="large" variant="outlined" onClick={handleSubmit} color="success">Отправить</Button>
					</CustomButtonsBox>
				</CustomModalBox>
			</CustomModal>
	);
};

export default CreatePost;