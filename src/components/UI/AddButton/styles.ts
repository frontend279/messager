import {Box, Fab, styled, Tooltip} from "@mui/material";

export const AddButtonFab = styled(Fab)(({theme}) => (
	{
		position: 'fixed',
		bottom: 20,
		left: 20,
		[theme.breakpoints.down('xs')]: {
			left: 'calc(50% - 25px)',
		}
	}
))