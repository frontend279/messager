import React from 'react';
import {Box, Fab} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import {AddButtonFab} from "./styles";
import {useAppDispatch} from "../../../redux/store";
import {CreatePostModalSliceActions} from "../../../redux/slices/modals/CreatePostSlice";
const AddButton = () => {

	const dispatch = useAppDispatch()
	const handleOpenModal = () => {
		dispatch(CreatePostModalSliceActions.setModalOpen(true))
	}
	
	return (
			<AddButtonFab onClick={handleOpenModal} color="primary">
				<AddIcon />
			</AddButtonFab>
	);
};

export default AddButton;