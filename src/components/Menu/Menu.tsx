import React from 'react';
import {ListItemButton, ListItemIcon, ListItemText, List, Box} from "@mui/material";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

const Menu = () => {
	return (
		<Box flex={2} sx={{display: {xs: 'none', sm: 'block'}}}>
			<Box position="fixed">
				<List component="nav">
					<ListItemButton>
						<ListItemIcon>
							<AccountCircleIcon/>
						</ListItemIcon>
						<ListItemText primary={'Профиль'}/>

					</ListItemButton>
					<ListItemButton>
						<ListItemIcon>
							<AccountCircleIcon/>
						</ListItemIcon>
						<ListItemText primary={'Сообщения'}/>
					</ListItemButton>
					<ListItemButton>
						<ListItemIcon>
							<AccountCircleIcon/>
						</ListItemIcon>
						<ListItemText primary={'Друзья'}/>
					</ListItemButton>
					<ListItemButton>
						<ListItemIcon>
							<AccountCircleIcon/>
						</ListItemIcon>
						<ListItemText primary={'Группы'}/>
					</ListItemButton><ListItemButton>
					<ListItemIcon>
						<AccountCircleIcon/>
					</ListItemIcon>
					<ListItemText primary={'Избранное'}/>
				</ListItemButton>
					<ListItemButton>
						<ListItemIcon>
							<AccountCircleIcon/>
						</ListItemIcon>
						<ListItemText primary={'Прочее'}/>
					</ListItemButton>
				</List>
			</Box>
		</Box>
	);
};

export default Menu;