import React, {useEffect, useMemo, useRef, useState} from 'react';
import {Box, CircularProgress} from "@mui/material";
import PostCard from "../PostCard/PostCard";
import {useAppDispatch, useAppSelector} from "../../redux/store";
import {fetchPostsWithPage} from "../../API";
import {LoadingDiv} from "./styles";
import {IPost} from "../../interface/IPost";
import {useIntersect} from "../../hooks/useIntersect";

const Posts = () => {

	const dispatch = useAppDispatch()
	const {data, isLoading, error} = useAppSelector(state => state.LoadPostsSlice)
	const {search} = useAppSelector(state => state.SearchSlice)
	const [page, setPage] = useState<number>(1)
	const [posts, setPosts] = useState<IPost[]>([])
	const [filteredPosts, setFilteredPosts] = useState<IPost[]>([])
	const lastElement = useRef<HTMLDivElement | null>(null)
	useEffect(() => {
		dispatch(fetchPostsWithPage({limit: 10, page}))
	}, [page])
	
	useIntersect(lastElement, true, isLoading, [filteredPosts], () => {
			if(filteredPosts.length > 0 && search === '') {
				setPage(page + 1)
			}
	})
	
	useEffect(() => {
		setPosts([...posts, ...data])
	}, [data])

	useMemo(() => {
		setFilteredPosts([...posts.filter(post => post.body.toLowerCase().includes(search.toLowerCase())
			|| post.title.toLowerCase().includes(search.toLowerCase()))])
	}, [search, posts])
	
	return (
		<Box flex={6}>
			{
				isLoading && <LoadingDiv>
              <CircularProgress color="inherit" />
          </LoadingDiv>
			}
			{filteredPosts.map((post) => (
				<PostCard key={post.id} title={post.title} body={post.body}/>
			))}
			<div ref={lastElement} style={{height: '20px'}}></div>
		</Box>
	);
};

export default Posts;