import {styled} from "@mui/material";

export const LoadingDiv = styled('div')({
	display: 'flex',
	justifyContent: 'center',
	marginTop: '2rem'
})
