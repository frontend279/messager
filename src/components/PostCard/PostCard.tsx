import React, {FC} from 'react';
import {
	Avatar,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	CardMedia,
	Collapse,
	IconButton,
	Typography
} from "@mui/material";
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import {ExpandMore} from "@mui/icons-material";


interface IPostCard {
		title: string;
		body: string;
}

const PostCard: FC<IPostCard> = ({title, body}) => {
	return (
		<Card sx={{ margin: 5 }}>
			<CardHeader
				avatar={
					<Avatar>
						R
					</Avatar>
				}
				action={
					<IconButton>
						<MoreVertIcon />
					</IconButton>
				}
				title={title}
				subheader="September 14, 2016"
			/>
			<CardMedia
				component="img"
				image="https://mui.com//static/images/cards/paella.jpg"
				alt="Paella dish"
			/>
			<CardContent>
				<Typography>
					{body}
				</Typography>
			</CardContent>
			<CardActions>
				<IconButton>
					<FavoriteIcon />
				</IconButton>
				<IconButton>
					<ShareIcon />
				</IconButton>
			</CardActions>
			<Collapse>
				<CardContent>
					<Typography paragraph>{body}</Typography>
				</CardContent>
			</Collapse>
		</Card>
	);
};

export default PostCard;