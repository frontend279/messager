import React, {useEffect, useRef, useState} from 'react';
import {AppBar, Avatar, Divider, IconButton, InputBase, Menu, MenuItem, Paper, Typography} from "@mui/material";
import {CustomToolbar, IconsBox, Search} from "./styles";
import MailIcon from '@mui/icons-material/Mail';
import NotificationsIcon from '@mui/icons-material/Notifications';
import {useDispatch} from "react-redux";
import {SearchSliceActions} from "../../redux/slices/SearchSlice";
import MailMessage from "../MailMessage";

const Navbar = () => {

	const [search, setSearch] = useState<string>('')
	const dispatch = useDispatch()
	const searchHandler = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
		setSearch(event.currentTarget.value)
	}

	useEffect(() => {
		dispatch(SearchSliceActions.setSearch(search))
	}, [search])

	const [isProfileMenuOpen, setProfileMenuOpen] = useState<boolean>(false)
	const [isAlertMenuOpen, setAlertMenuOpen] = useState<boolean>(false)
	const [isMailMenuOpen, setMailMenuOpen] = useState<boolean>(false)
	const profileMenuEl = useRef<HTMLButtonElement | null>(null)
	const alertMenuEl = useRef<HTMLButtonElement | null>(null)
	const mailMenuEl = useRef<HTMLButtonElement | null>(null)


	return (
		<AppBar position="sticky">
			<CustomToolbar>
				<Typography variant={'h6'} component={'p'}>
					OnlinePosts
				</Typography>
				<Search>
					<InputBase value={search} onChange={searchHandler} placeholder="Search..."></InputBase>
				</Search>
				<IconsBox>
					<IconButton sx={{display: {xs: 'none', sm: 'block'}}} ref={mailMenuEl} onClick={() => {
						setMailMenuOpen(true)
					}} color="inherit">
						<MailIcon/>
					</IconButton>
					<IconButton sx={{display: {xs: 'none', sm: 'block'}}} ref={alertMenuEl} onClick={() => {
						setAlertMenuOpen(true)
					}} color="inherit">
						<NotificationsIcon/>
					</IconButton>
					<IconButton ref={profileMenuEl} onClick={() => {
						setProfileMenuOpen(true)
					}}>
						<Avatar alt="username" src="/static/images/avatar/2.jpg"/>
					</IconButton>
				</IconsBox>
			</CustomToolbar>
			<Menu
				anchorEl={profileMenuEl.current}
				open={isProfileMenuOpen}
				disableScrollLock={true}
				onClose={() => {
					setProfileMenuOpen(false)
				}}
			>
				<MenuItem>Профиль</MenuItem>
				<MenuItem>Настройки</MenuItem>
				<MenuItem>Выйти</MenuItem>
			</Menu>
			<Menu
				anchorEl={alertMenuEl.current}
				open={isAlertMenuOpen}
				disableScrollLock={true}
				onClose={() => {
					setAlertMenuOpen(false)
				}}
			>
				<Paper sx={{width: 320, maxWidth: '100%', boxShadow: 'none'}}>
					<MenuItem>
						<Typography variant="h6" color="text.secondary">
							Уведомления
						</Typography>
					</MenuItem>
					<Divider/>
					<MenuItem>

					</MenuItem>
				</Paper>
			</Menu>
			<Menu
				anchorEl={mailMenuEl.current}
				open={isMailMenuOpen}
				disableScrollLock={true}
				onClose={() => {
					setMailMenuOpen(false)
				}}
			>
				<Paper sx={{maxWidth: '100%', boxShadow: 'none', right: 14}}>
					<MenuItem>
						<Typography variant="h6" color="text.secondary">
							Личные сообщения
						</Typography>
					</MenuItem>
					<Divider/>
					<MenuItem>
						<MailMessage/>
					</MenuItem>
				</Paper>
			</Menu>
		</AppBar>
	);
};

export default Navbar;