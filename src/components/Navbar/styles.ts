import styled from "@emotion/styled";
import Toolbar from '@mui/material/Toolbar';
import {lightBlue} from "@mui/material/colors";
import {Box} from "@mui/material";
import NotificationsIcon from '@mui/icons-material/Notifications';


export const CustomToolbar = styled(Toolbar)({
	display: 'flex',
	justifyContent: 'space-between',
	backgroundColor: 'darkcyan',
})

export const Search = styled('div')({
	width: '40%',
	backgroundColor: 'white',
	borderRadius: '10px',
	padding: '0 10px',
})

export const IconsBox = styled(Box)({
		display: 'flex',
		flexDirection: 'row',
})