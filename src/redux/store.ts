import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import {LoadPostsSlice, nameLoadPostsSlice} from "./slices/LoadPostsSlice";
import {nameSearchSlice, SearchSlice} from "./slices/SearchSlice";
import {
	CreatePostModalSlice,
	CreatePostModalSliceActions,
	nameModalCreatePostSlice
} from "./slices/modals/CreatePostSlice";


const rootReducer = combineReducers({
	[nameLoadPostsSlice]: LoadPostsSlice.reducer,
	[nameSearchSlice]: SearchSlice.reducer,
	[nameModalCreatePostSlice]: CreatePostModalSlice.reducer,
})

export const store = configureStore({
	reducer: rootReducer
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector