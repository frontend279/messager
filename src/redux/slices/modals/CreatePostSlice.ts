import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export const nameModalCreatePostSlice = 'ModalCreatePostSlice'

interface ICreatePostSlice {
	isOpen: boolean
}

const initialState: ICreatePostSlice = {
	isOpen: false,
}

export const CreatePostModalSlice = createSlice({
	name: nameModalCreatePostSlice,
	initialState,
	reducers: {
		setModalOpen: (state, action: PayloadAction<boolean>) => {
				state.isOpen = action.payload
		}
	},
	extraReducers: {}
})

export const CreatePostModalSliceActions = CreatePostModalSlice.actions