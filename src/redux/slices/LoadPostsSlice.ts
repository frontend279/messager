import {IPost} from "../../interface/IPost";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {fetchAllPosts, fetchPostsWithPage} from "../../API";

export const nameLoadPostsSlice = 'LoadPostsSlice'

interface ILoadPostsSlice {
	data: IPost[]
	isLoading: boolean,
	error: string,
}

const initialState: ILoadPostsSlice = {
	data: [],
	isLoading: false,
	error: ''
}

export const LoadPostsSlice = createSlice({
	name: nameLoadPostsSlice,
	initialState,
	reducers: {},
	extraReducers: {
		[fetchAllPosts.pending.type]: (state) => {
			state.isLoading = true
			state.data = []
			state.error = ''
		},
		[fetchAllPosts.fulfilled.type]: (state, action: PayloadAction<IPost[]>) => {
			state.isLoading = false
			state.data = action.payload
			state.error = ''
		},
		[fetchAllPosts.rejected.type]: (state, action: PayloadAction<string>) => {
			state.isLoading = false
			state.data = []
			state.error = action.payload
		},
		[fetchPostsWithPage.pending.type]: (state) => {
			state.isLoading = true
			state.data = []
			state.error = ''
		},
		[fetchPostsWithPage.fulfilled.type]: (state, action: PayloadAction<IPost[]>) => {
			state.isLoading = false
			state.data = action.payload
			state.error = ''
		},
		[fetchPostsWithPage.rejected.type]: (state, action: PayloadAction<string>) => {
			state.isLoading = false
			state.data = []
			state.error = action.payload
		},
	}
})

export const LoadPostsSliceActions = LoadPostsSlice.actions