import {IPost} from "../../interface/IPost";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {fetchAllPosts, fetchPostsWithPage} from "../../API";

export const nameSearchSlice = 'SearchSlice'

interface ILoadPostsSlice {
	search: string
}

const initialState: ILoadPostsSlice = {
	search: ''
}

export const SearchSlice = createSlice({
	name: nameSearchSlice,
	initialState,
	reducers: {
		setSearch: (state, action: PayloadAction<string>) => {
			state.search = action.payload
		}
	},
	extraReducers: {},
})

export const SearchSliceActions = SearchSlice.actions