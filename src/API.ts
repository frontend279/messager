import {createAsyncThunk} from "@reduxjs/toolkit";
import {IPost} from "./interface/IPost";
import axios from "axios";

export const fetchAllPosts = createAsyncThunk(
	'LoadPostsSlice/fetchAllPosts',
	async (limit: number, thunkAPI) => {
		try {
			const response =
				await axios.get<IPost[]>('https://jsonplaceholder.typicode.com/posts', {
					params: {
						'_limit': limit,
					}
				})
			return response.data
		} catch (e) {
			return thunkAPI.rejectWithValue('Ошибка загрузки')
		}
	}
)
interface IFetchPostsWithPage {
		limit: number,
		page: number
}

export const fetchPostsWithPage = createAsyncThunk(
	'LoadPostsSlice/fetchPostsWithPage',
	async (params: IFetchPostsWithPage, thunkAPI) => {
		try {
			const response =
				await axios.get<IPost[]>('https://jsonplaceholder.typicode.com/posts', {
					params: {
						'_limit': params.limit,
						'_page': params.page,
					}
				})
			return response.data
		} catch (e) {
			return thunkAPI.rejectWithValue('Ошибка загрузки')
		}
	}
)