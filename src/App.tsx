import Navbar from "./components/Navbar/Navbar";
import {Stack} from "@mui/material";
import Menu from "./components/Menu/Menu";
import Posts from "./components/Posts/Posts";
import RecentActions from "./components/RecentActions/RecentActions";
import AddButton from "./components/UI/AddButton/AddButton";
import CreatePost from "./components/Modals/CreatePost/CreatePost";

function App() {


	return (
		<div className="App">
			<Navbar/>
			<Stack direction="row" spacing={2} justifyContent="space-between">
				<Menu/>
				<Posts/>
				<RecentActions/>
			</Stack>
			<CreatePost />
			<AddButton/>
		</div>
	)
}

export default App
