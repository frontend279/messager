import {RefObject, useEffect, useLayoutEffect, useRef} from "react";

export const useIntersect = (ref: RefObject<HTMLElement>, canLoad: boolean, 
														 isLoading: boolean, deps: Array<any>, callback: Function) => {
	const observer = useRef<IntersectionObserver | null>(null);

	useEffect(() => {
		if(isLoading) return
		if(observer.current) observer.current.disconnect();

		var cb: IntersectionObserverCallback = function(entries) {
			if (entries[0].isIntersecting && canLoad) {
				callback()
			}
		};
		observer.current = new IntersectionObserver(cb);
		if(ref?.current) observer.current.observe(ref.current)
	}, deps)
}